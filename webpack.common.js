const path = require("path");
module.exports = {
  mode: "development",
  devtool: "none",
  entry: {
    main: "./src/js/index.js",
    vendor: "./src/js/vendor.js"
  },
  output: {
    filename: "main.[contentHash].js",
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
        test: /\.html$/,
        use: ['html-loader']
      }
      
    ],
  }
}