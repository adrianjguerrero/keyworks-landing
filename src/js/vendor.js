// fullpagejs
import 'fullpage.js/vendors/scrolloverflow'
import fullpage from 'fullpage.js';

document.addEventListener('DOMContentLoaded', function() {

    var myFullpage = new fullpage('#fullpage', {
        sectionSelector: '.seccion',
        scrollOverflow: true,
        paddingTop: '20px',
        verticalCentered: true
    });
  //   lightGallery(document.getElementById('lightgallery'));

    let btnNext = document.querySelector('.video-section__actions-mouse');

    let goTop = document.querySelector('.go-top');

    btnNext.addEventListener('click', () => {
        fullpage_api.moveSectionDown();
    });

    goTop.addEventListener('click', () => {
        fullpage_api.moveTo(1);
    });
})


// lightgallery
// var lightGallery = require('lightgallery.js');

// lightGallery( document.getElementById('lightgallery') );

// TODO: HACER QUE FUNCIONE LIGHTGALLERY DESDE WEBPACKSS

// import 'lightgallery/src/js/lightgallery';
// lightGallery(document.getElementById('lightgallery'));
// require(['./lightgallery.js'], function() {
    
// });

